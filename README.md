#MongoDB+Node.JS API Generic Server
a generic Restful API server implemented with:

* Express
* Node.JS
* MondoDB


Based on Ray Wenderlich article <http://www.raywenderlich.com/61078/write-simple-node-jsmongodb-web-service-ios-app>

##Setup and run

    #clone repo
    git clone https://bitbucket.org/osrufung/restapi-mongodb-nodejs.git

    #install Node Modules
    npm install

    #start MongoDB DB on localhost
    mongod  --dbpath ./testMongoDBData

    #start Node HTTP server
    node index.js
    
##CRUD example actions
### Insert element (POST /beacons)
    curl -H "Content-type: application/json" -X POST -d '{"uuid":"8705E790-EF93-46AE-9AB9-DCD080AC379F", "identifier":"Beacon 1"}' http://localhost:3000/beacons
    
 Returns a JSON Object like this

    {
    "uuid": "86D8F7781-0280-4D92-A7D1-13CD74FE87B5",
    "identifier": "Beacon 1",
    "created_at": "2014-05-13T19:10:15.327Z",
    "_id": "53726e17410eb29890329f75"
    }    

### Read all elements (GET /beacons)
     curl   -H "Content-type: application/json" -X GET http://localhost:3000/beacons
     
Returns all elements in *beacons* collection
    
    {
    "_id": "53726e17410eb29890329f75",
    "uuid": "86D8F7781-0280-4D92-A7D1-13CD74FE87B5",
    "identifier": "Beacon 1",
    "created_at": "2014-05-13T19:10:15.327Z"
    },
    {
    "_id": "53726eaa410eb29890329f76",
    "uuid": "8705E790-EF93-46AE-9AB9-DCD080AC379F",
    "identifier": "Beacon 2",
    "created_at": "2014-05-13T19:12:42.789Z"
    }

### Read element by _id (GET /beacons/{_id})

    curl   -H "Content-type: application/json" -X GET http://localhost:3000/beacons/5326e17410eb29890329f75
     
Returns individual element indicated by */beacons/{_id}* 


### Update element (PUT /beacons/{_id})
    curl   -H "Content-type: application/json" -X  PUT -d '{"identifier":"Beacon 1 updated"}'  http://localhost:3000/beacons/53726e17410eb29890329f75

Update document indicated by */beacons/{_id}* 

    {
    "identifier": "Beacon 1 updated",
    "_id": "53726e17410eb29890329f75",
    "updated_at": "2014-05-13T19:19:52.733Z"
   }
   

### Delete element (DELETE /beacons/{_id})
    curl   -H "Content-type: application/json" -X  DELETE  http://localhost:3000/beacons/53726e17410eb29890329f75
   
Delete individual element indicated by */beacons/{_id}* 
   

##Contact
* Oswaldo Rubio
* <osrufung@gmail.com>
* [@arrozconnori](http://twitter.com/arrozconnori)
